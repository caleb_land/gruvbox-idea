Gruvbox for IDEA IDEs<br/>(IntelliJ, RubyMine, WebStorm, etc)
=========================================================

This is a clone of the Gruvbox for IDEA IDEs repo maintained by <a href="https://github.com/caleb">caleb</a> on github.com

If you're on macOS or Linux run the included `install.bash` script to install the themes for all of your installed Jetbrians IDEs.

The install script should work for most IDEs. But if the theme doesn't appear after running the script perhaps the script has to be updated to include that IDE.

![Gruvbox for IDEA](https://raw.githubusercontent.com/caleb/gruvbox-idea/master/screenshot.png)
